// app.js
// BASE SETUP
// =============================================================================

// call the packages we need
// popular node framework
var express = require('express');
// lib to read and write from file system
var fs = require('fs');
// lib to make simple http requests
var request = require('request');
// used to traverse to dom
var cheerio = require('cheerio');
// used to parse xml to json needed for specific restaurants
var xml2js = require('xml2js');
// date library to format dates
var moment = require('moment');
// html entity encoder/decoder
var he = require('he');
// create new application
var app = express();

// setup first route
app.get('/scrape', function(req, res) {

	// first restaurant url to scrape
	url = 'http://menu.mensen.at/index/rss/locid/45';

	// The structure of our request call
	// The first parameter is our URL
	// The callback function takes 3 parameters, an error, response status code and the html
	request(url, function(error, response, xml) {

		// First we'll check to make sure no errors occurred when making the request
		if (!error) {

			// first we are setting up the json object, which we will shoot over to slack
			var json = {
				username: 'Sepp - Der Feinschmecker',
				icon_emoji: ':older_man::skin-tone-3:',
				channel: '#general'
			};

			// we are parsing the xml returned from the website
			xml2js.parseString(xml, function(err, result) {

				// replace unneccesary strings
				json.text = he.decode(result.rss.channel[0].item[0].description[0].replace(/(<([^>]+)>)/ig, ''));

				// finally post to slack
				request.post({
					url: 'https://hooks.slack.com/services/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',
					body: JSON.stringify(json)
				}, function(err, response) {
					// write erros to the file system
					if (err) {
						console.log(err);
						fs.writeFile('error.json', err);
					}
					if (response.body !== 'ok') {
						console.log(response);
						fs.writeFile('error.json', response);
					}
				});

				// Finally, we'll just send out a message to the browser reminding you that this app does not have a UI.
				res.send('Check your console!');
			});
		}
	});
});

// define on which port the app is listening
app.listen(process.env.PORT || '8081')
console.log('Magic happens on port 8081');
exports = module.exports = app;